import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import {primeraMayuscula} from '../helpers';

const ContenedorResumen = styled.div`
    padding:1rem;
    text-align:center;
    background-color: #00838F;
    color:#fff;
    margin-top: 1rem;
`;

const Resumen = ({ datos }) => {

    // Extraemos los datos 
    const { origen, year, plan } = datos;

    if (origen.trim() === '' || year.trim() === '' || plan.trim() === '') return null;

    return (
        <ContenedorResumen>
            <h2>Resumen de Cotización</h2>
            <ul>
                <li>Origen: {primeraMayuscula(origen)}</li>
                <li>Año del coche: {primeraMayuscula(year)}</li>
                <li>Plan: {primeraMayuscula(plan)}</li>
            </ul>
        </ContenedorResumen>
    );
}

Resumen.propTypes = {
    datos: PropTypes.object.isRequired
}

export default Resumen;