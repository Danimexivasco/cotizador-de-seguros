// Obtiene la difernecia de años
export function obtenerDiferenciaYear(year) {
    return new Date().getFullYear() - year;
}

// Calcula el total a pagar según el origen
export function calcularOrigen(origen) {
    let incremento;

    switch (origen) {
        case 'americano':
            incremento = 1.15;
            break;
        case 'europeo':
            incremento = 1.30;
            break;
        case 'asiatico':
            incremento = 1.05;
            break;

        default:
            break;
    }

    return incremento;
}

// Calcula el tipo de seguro
export function calcularPlan(plan){
    return (plan === 'basico') ? 1.20 : 1.50;
    // let incremento;

    // switch (plan) {
    //     case 'basico':
    //         incremento = 1.20;
    //         break;
    //     case 'completo':
    //         incremento = 1.50;
    //         break;
    
    //     default:
    //         break;
    // }
    // return incremento;
}


// Muestra la primera letra mayuscula en el resumen

export function primeraMayuscula(texto){
    return texto.charAt(0).toUpperCase() + texto.slice(1);
}